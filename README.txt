Advanced animate on scroll
======

The advanced animate on scroll module allows you to include animations to the
fields of your content.

Each field can have its own animation independent of the rest.

Once the animation is enabled, you just have to select the animation to apply
in the list.

We are working with the most stable version of AOS, but we do not rule out
using version 3 when it is stable.

The javascript library to provide the animations are the work of michalsnik.

For more information about the library you can consult the original project:
https://github.com/michalsnik/aos

Installing Libraries
---------------------

Not necessary anymore.

Enabling animation for one field
-------------------------

Advanced animation on scroll creates new format settings into 'Manage display'
tab. It can be applied to any entity which could be managed on display.

To enable an animation for a field, user only has to click on format
configuration link and check 'Enable Scroll Animations'. He will have 3
options to manage the animation:
- Select animation type.
- Add a duration of the animation.
- Add a delay of the animation.

Support
-------

For bug reports and feature requests please use the Drupal.org issue tracker:
http://drupal.org/project/issues/adv_animate_on_scroll.
